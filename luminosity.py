import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
import os
import re
import scipy.stats
import sys
from tabulate import tabulate

px = 1 / plt.rcParams["figure.dpi"]  # pixel size in inches, default is 0.01
defaultcolors = plt.rcParams["axes.prop_cycle"].by_key()["color"]

# these are strings in order to display them exactly this way
MIN_T4 = "10e11"
MIN_T10 = "5e11"
MIN_ARGONION = "0.5e9"
MIN_SIZE_MB = "200"
MAX_DELTA_T_SECONDS = "120"
MAX_DELTA_T_SECONDS_PLOT = 120

PLOT_SIZE = (800 * px, 600 * px)  # size of T4/T10 plot. px = pixels (otherwise, units are inches)
PLOT_LABEL_SIZE = "x-large"  # also accepts inches
PLOT_TIME_FORMAT = "%d/%m %H:%M"  # strftime format
PLOT_MAJOR_LOCATOR = mdates.HourLocator([0, 12])  # grid points at 00:00 and 12:00
PLOT_MINOR_LOCATOR = mdates.HourLocator(
    range(0, 24, 2)
)  # tick points every two hours starting from 00:00
BEAM_PLOT_HEIGHT_RATIOS = (2, 1, 1)
NA62_PLOT_HEIGHT_RATIOS = (2, 1)
T4_PLOT_COLOR = defaultcolors[0]  # or accepts hex rgb, example '#ff00ff'
T10_PLOT_COLOR = defaultcolors[1]
T10_INTENSITY_PLOT_COLOR = defaultcolors[2]
ARGONION_INTENSITY_PLOT_COLOR = defaultcolors[3]
DT_PLOT_COLOR = defaultcolors[4]
FILES_PLOT_COLOR = defaultcolors[2]
TB_PLOT_COLOR = defaultcolors[3]
NA62AVAILABILITY_PLOT_COLOR = defaultcolors[4]

T10_INTENSITY_BIN = timedelta(minutes=15)
ARGONION_PER_T10 = 0.1  # adjusts scaling of ARGONION axis
TB_PER_FILE = 5e-3  # adjusts scaling of TB axis

ARGONION_CALIBRATION_CONSTANTS = {
    # map timestamp of beginning period to calibration consant
    0: 1e4,
    1715162400: 6700,
}


def parse_args(args: list[str]):
    script_file = args.pop(0)

    def print_help_and_exit():
        print(
            f"Usage: python3 {script_file} <path to timber data> <output path of beam plot>.png <output path of na62 plot>.png"
        )
        sys.exit(0)

    verbose = "-v" in args or "--verbose" in args
    if verbose:
        args = [arg for arg in args if arg != "-v" and arg != "--verbose"]

    try:
        timber_csvfile = args.pop(0)
        if "-h" in args or "--help" in args:
            print_help_and_exit()
    except IndexError:
        print_help_and_exit()

    beamplot_outputpath = args.pop(0) if args else None
    na62plot_outputpath = args.pop(0) if args else None

    return verbose, timber_csvfile, beamplot_outputpath, na62plot_outputpath


def read_timber_data(csv_filepath: str):
    """Read file exported from timber and return a pandas dataframe"""
    print(f"Reading {csv_filepath}")
    df = pd.read_csv(csv_filepath)
    df.rename(
        {
            "Timestamp (UNIX_TIME)": "Timestamp",
            "SPS.T10:INTENSITY": "T10",
            "SPS.T4:INTENSITY": "T4",
            "XBP42.XION.101.265:COUNTS": "ARGONION",
        },
        axis=1,
        inplace=True,
    )
    assert df["Timestamp"].is_monotonic_increasing  # check it's chronologically ordered
    df["Timestamp"] = df["Timestamp"] / 1e6  # weird units on TIMBER... make sure column remains int64
    df["Timestamp"] = df["Timestamp"].astype(int)

    # apply calibration constants in argonion
    period_starts = ARGONION_CALIBRATION_CONSTANTS.keys()
    period_ends = list(ARGONION_CALIBRATION_CONSTANTS.keys())[1:] + [float("inf")]
    for period_start, period_end in zip(period_starts, period_ends):
        period = (df["Timestamp"] > period_start) & (df["Timestamp"] < period_end)
        df.loc[period, "ARGONION"] = (
            df.loc[period, "ARGONION"] * ARGONION_CALIBRATION_CONSTANTS[period_start]
        )

    return df


def get_files_df(mintimestamp, maxtimestamp):
    """Look for files on EOS online and return a pandas dataframe with info on files"""

    print("Looking for files on EOS...")
    EOSONLINE = "/eos/experiment/na62/data/online"
    rundfs = []
    # don't check runs older than the mintimestamp
    rundirs = [
        EOSONLINE + "/" + runnr
        for runnr in os.listdir(EOSONLINE)
        if mintimestamp < os.path.getmtime(EOSONLINE + "/" + runnr) and runnr != "000000" and runnr != "000001"
    ]
    # the other way round is trickier because ctime doesn't work... so we'll uselessly check runs that are obviously newer
    for rundir in rundirs:
        burstlist = []
        for datfile in os.listdir(rundir):
            # extract SOB timestamp, run number and burst number from the filename
            m = re.match(r"na62raw_(\d+)-0[1-4]-(\d+)-(\d+).dat", datfile)
            if not m:
                continue
            ts = m[1]
            ts = int(ts) if ts != "0" else None  # some timestamps are 0...
            if ts and not mintimestamp < ts < maxtimestamp:
                continue  # skip files that aren't in the requested time range
            run = m[2]
            burst = m[3]
            size = os.path.getsize(rundir + "/" + datfile) / 1e6  # size in MB
            assert run == os.path.basename(rundir)  # consistency check
            burstlist.append((int(ts) if ts else None, int(run), int(burst), size))
        rundf = pd.DataFrame(
            sorted(burstlist, key=lambda x: x[2]), columns=["FileTS", "Run", "Burst", "Size"]
        )

        # remove empty folders: modification date likely due to copy of data to CTA
        if len(rundf)>0:
            rundfs.append(rundf)

    return pd.concat(rundfs)


def sanitize_files_df(filesdf: pd.DataFrame):
    """Some checks and sanitizing of files data"""

    # don't really care about the default index
    filesdf.reset_index(drop=True, inplace=True)

    # a few timestamps aren't chronologically ordered... just remove their timestamp and replace with NaN
    replaced = 0
    while not filesdf["FileTS"][filesdf["FileTS"].notna()].is_monotonic_increasing:
        nonmonotonic = (
            filesdf["FileTS"].ffill().diff() < 0
        )  # whether the difference between the TS and the non-NaN following is negative
        replaced += sum(nonmonotonic)
        filesdf.loc[nonmonotonic, "FileTS"] = pd.NA
    print(f"Found {replaced} non monotonic timestamps, replaced with NaN")

    # consistency check of orderings
    assert (
        filesdf["FileTS"].dropna().is_monotonic_increasing
    )  # sorted by time (ignoring NaN timestamps)
    assert (
        filesdf["Run"] * 10000 + filesdf["Burst"]
    ).is_monotonic_increasing  # sorted by run & burst

    # remove first and last NaNs (most likely they correspond to files outside of the requested range)
    filesdf.drop(range(0, filesdf["FileTS"].first_valid_index()), inplace=True)
    filesdf.drop(range(filesdf["FileTS"].last_valid_index() + 1, len(filesdf)), inplace=True)

    # guess missing timestamps by interpolating linearly
    print(f'{sum(filesdf["FileTS"].isna())} file timestamps are NaN. Adding interpolated column')
    filesdf = filesdf.assign(InterpolatedTS=filesdf["FileTS"].interpolate(method="linear"))

    # this should make next merges quicker
    filesdf.set_index("InterpolatedTS")

    return filesdf


def match_via_timestamp(timberdf: pd.DataFrame, filesdf: pd.DataFrame):
    """Get one dataframe with all the data"""

    # take each file and match it with the timber record that has the timestamp closest in time (but backwards) to the interpolatedTS
    # (the timestamp in the file is the SOB, and has s precision, while the corresponding timestamp in timber precedes the SOB of about 3 s and has ms precision)
    fulldf = pd.merge_asof(
        filesdf, timberdf, left_on="InterpolatedTS", right_on="Timestamp", direction="backward"
    )

    # add all the timber records that don't correspond to any file
    fulldf = pd.merge(timberdf, fulldf, how="left")
    return fulldf


def calculate_duty_cycle(timestamps: pd.Series, selection: pd.Series):
    if selection is not None:
        timestamps = timestamps[selection]
        ntotal = sum(selection)
    else:
        ntotal = len(timestamps)
    dt = timestamps.diff()
    return (
        ntotal,
        (timestamps.iloc[-1] - timestamps.iloc[0]) / (ntotal - 1),
        dt[dt < float(MAX_DELTA_T_SECONDS)].mean(),
    )


def print_table(timberdf: pd.DataFrame, matchedfilesdf: pd.DataFrame, verbose: bool):
    def duty_cycle(timestamps: pd.Series, selection: pd.Series):
        ntotal, meandt, meandt_restricted = calculate_duty_cycle(timestamps, selection)
        if verbose:
            return ntotal, "{:.1f} s".format(meandt), "{:.1f} s".format(meandt_restricted)
        else:
            return (ntotal,)

    def pretty_print_table(table):
        print(tabulate(table, tablefmt="plain"))

    timber_ts = timberdf["Timestamp"]
    files_ts = matchedfilesdf["Timestamp"]

    table1 = [
        ["Period start:", datetime.fromtimestamp(timber_ts.iloc[0]).strftime("%c")],
        ["Period end:", datetime.fromtimestamp(timber_ts.iloc[-1]).strftime("%c")],
        ["Total seconds: ", "{:.1f}".format(timber_ts.iloc[-1] - timber_ts.iloc[0])],
        ["Total POT on T4:", "{:.4e}".format(timberdf["T4"].sum())] if verbose else [],
        ["Total POT on T10:", "{:.4e}".format(timberdf["T10"].sum())],
    ]

    table2 = [
        ["", "Total", "<ΔT>", f"<ΔT> for ΔT < {MAX_DELTA_T_SECONDS} s"] if verbose else [],
        ["SOBs:", *duty_cycle(timber_ts, None)],
        [
            f"T4 non-empty bursts (>{MIN_T4}):",
            *duty_cycle(timber_ts, timberdf["T4"] > float(MIN_T4)),
        ],
        [
            f"T10 non-empty bursts (>{MIN_T10}):",
            *duty_cycle(timber_ts, timberdf["T10"] > float(MIN_T10)),
        ],
        (
            [
                f"ARGONION non-empty bursts (>{MIN_ARGONION}):",
                *duty_cycle(timber_ts, timberdf["ARGONION"] > float(MIN_ARGONION)),
            ]
            if verbose
            else []
        ),
        ["Bursts on disk: ", *duty_cycle(files_ts, None)],
        [
            "T10 non-empty bursts on disk:",
            *duty_cycle(files_ts, matchedfilesdf["T10"] > float(MIN_T10)),
        ],
        [
            f"Non-empty files on disk (>{MIN_SIZE_MB} MB):",
            *duty_cycle(files_ts, matchedfilesdf["Size"] > float(MIN_SIZE_MB)),
        ],
    ]

    if verbose:
        pretty_print_table(table1)
        print()
        pretty_print_table(table2)
    else:
        pretty_print_table([row for row in table1 + table2 if row])
    print()
    print()


def make_figure(dates, t4, t10, height_ratios, xmin=None, xmax=None):
    """Creates the multi-figure. Pass xmin and xmax to apply them, leave them None to use the default ones. Returns the figure, the axes and (xmin, xmax)"""
    fig = plt.figure(figsize=(PLOT_SIZE[0], PLOT_SIZE[1] * sum(height_ratios) / height_ratios[0]))
    gs = fig.add_gridspec(len(height_ratios), height_ratios=height_ratios, hspace=0)
    axs = gs.subplots(sharex=True)
    axs[0].plot(dates, t4, label="T4", c=T4_PLOT_COLOR)
    axs[0].plot(dates, t10, label="T10", c=T10_PLOT_COLOR)
    axs[0].legend()
    axs[0].xaxis.set_major_locator(PLOT_MAJOR_LOCATOR)
    axs[0].xaxis.set_minor_locator(PLOT_MINOR_LOCATOR)
    axs[0].xaxis.set_major_formatter(mdates.DateFormatter(PLOT_TIME_FORMAT))
    axs[0].set_ylabel("POT")
    if xmin and xmax:
        axs[0].set_xlim(xmin, xmax)
    for ax in axs:
        ax.grid(visible=True)
        ax.grid(visible=True, which="minor", axis="x", linestyle="--", alpha=0.3)
        ax.label_outer()
    return fig, axs, axs[0].get_xlim()


def get_mdates(timestamps: pd.Series):
    """Convert a pd.Series of (numeric) timestamps to internal matplotlib representation of dates"""
    return mdates.date2num([datetime.fromtimestamp(ts) for ts in timestamps])


def make_beam_plot(timberdf: pd.DataFrame, dates, t4, t10):
    fig, axs, (xmin, xmax) = make_figure(dates, t4, t10, BEAM_PLOT_HEIGHT_RATIOS)

    timestamps = timberdf["Timestamp"]
    potbins = np.arange(timestamps.iloc[0], timestamps.iloc[-1], T10_INTENSITY_BIN.total_seconds())
    bindates = get_mdates(potbins[:-1])

    def make_binned_average(series: pd.Series):
        return scipy.stats.binned_statistic(timestamps, series, bins=potbins).statistic

    # plot T10
    axs[1].plot(
        bindates,
        make_binned_average(timberdf["T10"] / 1e11),
        marker=".",
        c=T10_INTENSITY_PLOT_COLOR,
        label="T10/E11",
    )
    axs[1].set_ylabel("Avg intensity")
    axs[1].yaxis.set_tick_params(colors=T10_INTENSITY_PLOT_COLOR)

    # plot ARGONION (on same axis, rescaled)
    axs[1].plot(
        bindates,
        make_binned_average(timberdf["ARGONION"] / 1e9) / ARGONION_PER_T10,
        marker=".",
        c=ARGONION_INTENSITY_PLOT_COLOR,
        label="ARGONION/E9",
    )
    axs[1].legend()

    # make argonion axis for scale
    argonionax = axs[1].twinx()
    argonionax.grid(visible=False)
    argonionax.set_ylim([y * ARGONION_PER_T10 for y in axs[1].get_ylim()])
    argonionax.yaxis.set_tick_params(colors=ARGONION_INTENSITY_PLOT_COLOR)

    dt = timestamps.diff()
    axs[2].scatter(
        dates[dt < MAX_DELTA_T_SECONDS_PLOT],
        dt[dt < MAX_DELTA_T_SECONDS_PLOT],
        s=1,
        c=DT_PLOT_COLOR,
    )
    axs[2].set_ylabel("ΔT(SOB) (s)")

    return fig


def make_na62_plot(matchedfilesdf: pd.DataFrame, fulldf: pd.DataFrame, dates, t4, t10):
    fig, axs, _ = make_figure(dates, t4, t10, NA62_PLOT_HEIGHT_RATIOS)
    matchedfilesdates = get_mdates(matchedfilesdf["Timestamp"])
    axs[1].plot(
        matchedfilesdates, matchedfilesdf.index.values, label="Files on disk", c=FILES_PLOT_COLOR
    )
    axs[1].yaxis.set_tick_params(colors=FILES_PLOT_COLOR)

    tbax = axs[1].twinx()  # for TB plot
    tbax.plot(
        matchedfilesdates,
        matchedfilesdf["Size"].cumsum() / 1e6,
        label="TB on disk",
        c=TB_PLOT_COLOR,
    )
    tbax.grid(visible=False)
    tbax.set_ylim([y * TB_PER_FILE for y in axs[1].get_ylim()])
    tbax.yaxis.set_tick_params(colors=TB_PLOT_COLOR)

    na62nonavailable = fulldf["Burst"].isna() & (fulldf["T10"] > float(MIN_T10))
    tbax.scatter(
        get_mdates(fulldf["Timestamp"][na62nonavailable]),
        np.full(sum(na62nonavailable), 0),
        s=2,
        label=f"T10>{int(float(MIN_T10)/1e11)} but no file on disk",
        c=NA62AVAILABILITY_PLOT_COLOR,
    )
    (lines, labels), (lines2, labels2) = [ax.get_legend_handles_labels() for ax in (axs[1], tbax)]
    axs[1].legend(lines + lines2, labels + labels2)

    return fig


if __name__ == "__main__":
    verbose, timber_csvfile, beamplot_outputpath, na62plot_outputpath = parse_args(sys.argv[:])

    timberdf = read_timber_data(timber_csvfile)
    mintimestamp = timberdf["Timestamp"].iloc[0]
    maxtimestamp = timberdf["Timestamp"].iloc[-1]
    print(
        f"Found {len(timberdf)} TIMBER records between {datetime.fromtimestamp(mintimestamp)} and {datetime.fromtimestamp(maxtimestamp)}"
    )

    filesdf = get_files_df(mintimestamp, maxtimestamp)
    print(f"Found {len(filesdf)} files in this period")
    filesdf = sanitize_files_df(filesdf)
    print()

    # create a dataframe with all timber records, and the matching file data if the burst was recorded (otherwise NaN)
    fulldf = match_via_timestamp(timberdf, filesdf)

    # create a dataframe with all files with their corresponding timber records
    # fulldf, minus the lines that don't have run or burst
    matchedfilesdf = fulldf.dropna(
        subset=["Run", "Burst"]
    )  # could be created without passing through fulldf, but since we have it...
    matchedfilesdf.reset_index(drop=True, inplace=True)  # reset the index
    assert len(matchedfilesdf) == len(filesdf)  # consistency check

    print_table(timberdf, matchedfilesdf, verbose)

    plt.rcParams["axes.labelsize"] = PLOT_LABEL_SIZE

    dates, t4, t10 = (
        get_mdates(timberdf["Timestamp"]),
        timberdf["T4"].cumsum(),
        timberdf["T10"].cumsum(),
    )  # save them, don't recalculate
    fig = make_beam_plot(timberdf, dates, t4, t10)
    if beamplot_outputpath:
        fig.savefig(beamplot_outputpath)
    else:
        fig.show()
    fig = make_na62_plot(matchedfilesdf, fulldf, dates, t4, t10)
    if na62plot_outputpath:
        fig.savefig(na62plot_outputpath)
    else:
        fig.show()
