# Installation

Make a python virtual environment and activate it (this is not required, but very useful not to have dependency conflicts among other different python versions that you have installed):

```bash
$ python3 -m venv luminosityvenv # change luminosityvenv with any (non-existing) path you want
$ source luminosityvenv/bin/activate
```

Then install the dependencies:

```bash
(luminosityvenv) $ pip install --upgrade pip
(luminosityvenv) $ pip install -r requirements.txt
```

This installs a precise set of package versions that has been tested on lxplus7 and on lxplus9. Should it fail on any other machine, a simple

```bash
(luminosityvenv) $ pip install matplotlib numpy pandas scipy tabulate
```

is worth a shot.

Packages will be installed in this virtual environment only. You can run 

```bash
(luminosityvenv) $ deactivate
```

when you're done using this virtual environment.

Notice you also need `/eos/experiment` mounted on the machine you run this code on.

# Usage

### Downloading data from Timber

(unfortunately I couldn't manage to make `pytimber` work, which could have automatized the process)

1. Go to [timber.cern.ch](http://timber.cern.ch)
1. In the right side panel, choose a time window  
    N.B. You're entering UTC times in the form! So you will need to subtract 2 hours from the actual times that you want.
1. Add the following variables (via _Add variable_ under _Selected variables_, then searching by name and ticking them):
   - `SPS.T10:INTENSITY` (ppp on T10)
   - `SPS.T4:INTENSITY` (ppp on T4)
    - `XBP42.XION.101.265:COUNTS` (counts in ARGONION)
1. Click on _File_, then select _Group by timestamp_ and _Time in UNIX format_ (make sure that _File format_ is _Comma delimited_)
1. Click on _Load_, then save the file

### Running the code

First check settings in the beginning of `luminosity.py`, then
```bash
source luminosityvenv/bin/activate # substitute luminosityvenv with the path to your python virtual environment
(luminosityvenv) $ python luminosity.py <path to timber data> <output path of beam plot>.png <output path of na62 plot>.png
```
Add `-v` or `--verbose` to the last command to print out more stats in the table.

### Jupyter notebook

A simple notebook is also provided with the main function of `luminosity.py` made interactive.